from PyLogger.Logger import logger

from cicd_posting_bot.cfg import Cfg


def text_handler(bot, payload):
    try:
        logger.info(f"Message type: {payload.message.content_type}")
        logger.message(f"Message text: {payload.message.html_text}")

        _channel_text = f"{payload.default_text}\n{payload.message.html_text}"
        resp = bot.send_message(chat_id=Cfg.CHANNEL_ID, text=_channel_text)
        logger.message(f"Response from telgram API:\n{resp}")
        return resp

    except Exception as e:
        logger.warning(e)


def caption_handler(bot, payload):
    try:
        logger.info(f"Message type: {payload.message.content_type}")
        logger.message(f"Message text:: {payload.message.html_caption}")

        _channel_text = f"{payload.default_text}\n{payload.message.html_caption}"
        options = {
            "chat_id": Cfg.CHANNEL_ID,
            "caption": _channel_text,
        }

        if payload.message.content_type == "photo":
            options["photo"] = payload.message.photo[-1].file_id
            logger.message(f"Options: {options}")
            __handle(bot.send_photo, options)

        if payload.message.content_type == "document":
            options["data"] = payload.message.document.file_id
            logger.message(f"Options: {options}")
            __handle(bot.send_document, options)

    except Exception as e:
        logger.warning(e)


def __handle(handler, options):
    try:
        resp = handler(**options)
        logger.message(f"Response from telgram API:\n{resp}")
        return resp
    except Exception as e:
        logger.warning(e)
