import os

from PyLogger.Logger import logger

from cicd_posting_bot.vault import Worker

VAULT_ROLE_ID = os.getenv("VAULT_ROLE_ID")
VAULT_SECRET_ID = os.getenv("VAULT_SECRET_ID")

logger.info("Init VaultWorker")
vw = Worker(role_id=VAULT_ROLE_ID, secret_id=VAULT_SECRET_ID)


class Cfg(object):
    TOKEN = vw.get_secret("devcloud", "tg_bots/cicd_posting_bot").get(
        "BOT_TOKEN"
    )
    CHANNEL_ID = vw.get_secret("devcloud", "tg_bots/cicd_posting_bot").get(
        "CHANNEL_ID"
    )


class OpsGenieData(object):
    previous_duty = ""
    api_token = vw.get_secret("devcloud", "opsgenie").get("API_READ_TOKEN")
    api_url = "https://api.eu.opsgenie.com/v2/schedules/DevOps_schedule/on-calls?scheduleIdentifierType=name&flat=true"
    recipents = {
        "as.papichev": ("Александр Папичев", "papichev"),
    }
