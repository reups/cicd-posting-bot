import json
import requests

from PyLogger.Logger import logger

from cicd_posting_bot.cfg import OpsGenieData


def whos_on_call():
    try:
        logger.info("Получаем дежурного из OpsGenie...")
        res = requests.get(
            url=OpsGenieData.api_url,
            headers={"Authorization": f"Basic {OpsGenieData.api_token}"},
        )

        if res.ok:
            data = json.loads(res.content)
            on_call = data["data"]["onCallRecipients"][0]
            duty = OpsGenieData.recipents.get(on_call)
            OpsGenieData.previous_duty = duty
            logger.success(f"Дежурный получен: {duty}")
            return duty

        logger.message(
            f"Взят предыдущий дежурный: {OpsGenieData.previous_duty}"
        )
        return OpsGenieData.previous_duty

    except Exception as e:
        logger.warning(e)
