import telebot
from PyLogger.Logger import logger

logger.info("Starting bot...")

import cicd_posting_bot.handlers as handlers
from cicd_posting_bot.cfg import Cfg
from cicd_posting_bot.libs import Payload

logger.success("Bot started!")
bot = telebot.TeleBot(token=Cfg.TOKEN, parse_mode="HTML")


def __is_member(user_id) -> bool:
    """Checks if user is member of channel

    :param user_id: user id, who contacts with bot
    :type user_id: str | int
    :return: result of check: user is member of CICD channel
    :rtype: bool
    """
    try:
        logger.info(f"Check user {user_id} is member...")

        status = bot.get_chat_member(Cfg.CHANNEL_ID, user_id).status
        logger.message(f"User {user_id} status: {status}")

        if status not in ["creator", "administrator", "member"]:
            return False
        return True

    except Exception as e:
        logger.warning(e)
        logger.warning(f"Can't resolve status of {user_id}")
        return False


def __validate_message(message) -> bool:
    """PLACEHOLDER, now only ignore '/start' message

    :param message: user message content
    :type message: str
    :return: result of validation
    :rtype: bool
    """
    try:
        if message.text and message.text == "/start":
            logger.message(f"@{message.from_user.username} message == /start")
            return False
    except Exception as e:
        logger.warning(e)
        return False

    return True


@bot.message_handler(content_types=["photo", "text", "document"])
def post(message: str) -> None:
    """Post a new message in channel .

    :param message: user message content
    :type message: str
    """
    if not __validate_message(message):
        logger.warning("message not valid")
        return

    try:
        logger.info("New message.")
        logger.info(
            f"From user: {message.from_user.id}/@{message.from_user.username}/{message.from_user.full_name}."
        )
    except Exception as e:
        logger.warning(e)

    if not __is_member(message.from_user.id):
        try:
            logger.info("Reply to non channel member")
            bot.reply_to(
                message,
                f"{message.from_user.username} не состоит в канале CI/CD",
            )
            return

        except Exception as e:
            logger.warning(e)
            return

    try:
        logger.info("Generating payload...")

        payload = Payload(message)
        logger.message(f"Payload: {payload}")
    except Exception as e:
        logger.warning(e)
        return

    if message.content_type in ("photo", "document") and message.caption:
        handlers.caption_handler(bot, payload)

    elif message.content_type == "text":
        handlers.text_handler(bot, payload)

    else:
        try:
            logger.info("Reply, for wrong message")
            bot.reply_to(
                message,
                "Сообщения, не содержащее текста, или содержащие несколько документов - отправлять запрещено.",
            )
        except Exception as e:
            logger.warning(e)


def run():
    logger.info("Bot started")
    bot.polling()


if __name__ == "__main__":
    run()
