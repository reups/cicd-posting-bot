from cicd_posting_bot.opsgenie.worker import whos_on_call


class Payload(object):
    def __init__(self, message):
        self.sender_id = message.from_user.id
        self.sender_fullname = message.from_user.full_name
        self.sender_username = message.from_user.username
        self.duty_username = whos_on_call()
        self.message = message
        self.default_text = (
            f"От: {self.sender_fullname} @{self.sender_username}\n"
        )

        # TODO: Заготовка к моменту, когда будет парсер planimum и opsgenie
        # self.default_text = f"От: {self.sender_fullname} @{self.sender_username}\nДежурный: @{self.duty_username}\n"
