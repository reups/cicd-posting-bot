from setuptools import find_packages, setup
import os

setup(
    name="cicd-posting-bot",
    description="CI tools",
    version="1.0.1",
    url=os.getenv("CI_JOB_URL"),
    author=os.getenv("GITLAB_USER_NAME"),
    author_email=os.getenv("GITLAB_USER_EMAIL"),
    license="MIT",
    packages=find_packages(include=["cicd_posting_bot", "cicd_posting_bot.*"]),
    install_requires=[
        "certifi==2021.5.30",
        "charset-normalizer==2.0.4; python_version >= '3'",
        "colorama==0.4.4; sys_platform == 'win32'",
        "hvac==0.11.0",
        "idna==3.2; python_version >= '3'",
        "loguru==0.5.3; python_version >= '3.5'",
        "pytelegrambotapi==4.0.1",
        "requests==2.26.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'",
        "pylogger==1.0.0",
        "six==1.16.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'",
        "urllib3==1.26.6; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4' and python_version < '4'",
        "win32-setctime==1.0.3; sys_platform == 'win32'",
    ],
    keywords=["", ""],
    entry_points={
        "console_scripts": ["cicd-posting-bot=cicd_posting_bot.app:run"],
    },
)
